package com.example.webrtcstream.ui.screens.start

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.webrtcstream.R
import com.example.webrtcstream.webrtc.WebRTCSessionState

@Composable
fun StartScreen(
    state: WebRTCSessionState,
    onJoinCall: () -> Unit
) {
    Box(modifier = Modifier.fillMaxSize()) {
        var callBtnEnabled by remember { mutableStateOf(false) }

        val text = when (state) {
            WebRTCSessionState.Offline -> {
                callBtnEnabled = false
                stringResource(id = R.string.button_start_session)
            }
            WebRTCSessionState.Impossible -> {
                callBtnEnabled = false
                stringResource(id = R.string.session_impossible)
            }
            WebRTCSessionState.Ready -> {
                callBtnEnabled = true
                stringResource(id = R.string.session_ready)
            }
            WebRTCSessionState.Creating -> {
                callBtnEnabled = true
                stringResource(id = R.string.session_creating)
            }
            WebRTCSessionState.Active -> {
                callBtnEnabled = false
                stringResource(id = R.string.session_active)
            }
        }

        Button(
            modifier = Modifier.align(Alignment.Center),
            enabled = callBtnEnabled,
            onClick = { onJoinCall.invoke() }
        ) {
            Text(
                text = text,
                fontSize = 26.sp,
                fontWeight = FontWeight.Black
            )
        }

    }
}
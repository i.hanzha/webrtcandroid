package com.example.webrtcstream.ui.screens.video

data class CallMediaState(
    val isMicrophoneEnabled: Boolean = true,
    val isCameraEnabled: Boolean = true,
)
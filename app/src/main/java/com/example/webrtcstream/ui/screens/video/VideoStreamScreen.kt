package com.example.webrtcstream.ui.screens.video

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.unit.IntSize
import com.example.webrtcstream.ui.components.VideoRenderer
import com.example.webrtcstream.webrtc.sessions.LocalWebRtcSessionManager

@Composable
fun VideoStreamScreen() {

    val sessionManager = LocalWebRtcSessionManager.current

    LaunchedEffect(key1 = Unit) {
        sessionManager.onSessionScreenReady()
    }

    Box(modifier = Modifier.fillMaxSize()) {
        var parentSize: IntSize by remember { mutableStateOf(IntSize(0, 0)) }

        val remoteVideoTrackState by sessionManager.remoteVideoTrackFlow.collectAsState(null)
        val removeVideoTrack = remoteVideoTrackState

        val localVideoTrackState by sessionManager.localVideoTrackFlow.collectAsState(null)
        val localVideoTrack = localVideoTrackState

        val callMediaState by remember { mutableStateOf(CallMediaState()) }

        if (removeVideoTrack != null) {
            Column(modifier = Modifier.fillMaxSize()) {
                VideoRenderer(videoTrack = removeVideoTrack,
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f)
                        .onSizeChanged { parentSize = it })
                Box(modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)) {

                }
            }

        }
    }

}
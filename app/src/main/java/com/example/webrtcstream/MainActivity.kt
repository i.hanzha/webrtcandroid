package com.example.webrtcstream

import android.Manifest
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.webrtcstream.ui.screens.start.StartScreen
import com.example.webrtcstream.ui.screens.video.VideoStreamScreen
import com.example.webrtcstream.ui.theme.WebRTCStreamTheme
import com.example.webrtcstream.webrtc.SignalingClient
import com.example.webrtcstream.webrtc.peer.StreamPeerConnectionFactory
import com.example.webrtcstream.webrtc.sessions.LocalWebRtcSessionManager
import com.example.webrtcstream.webrtc.sessions.WebRtcSessionManager
import com.example.webrtcstream.webrtc.sessions.WebRtcSessionManagerImpl

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO), 0)

        val sessionManager: WebRtcSessionManager = WebRtcSessionManagerImpl(
            context = this,
            signalingClient = SignalingClient(),
            peerConnectionFactory = StreamPeerConnectionFactory(this)
        )

        setContent {
            WebRTCStreamTheme {
                CompositionLocalProvider(LocalWebRtcSessionManager provides sessionManager) {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colors.background
                    ) {
                        var onCallScreen by remember { mutableStateOf(false) }
                        val state by sessionManager.signalingClient.sessionStateFlow.collectAsState()

                        if (!onCallScreen) {
                            StartScreen(state = state) { onCallScreen = true }
                        } else {
                            VideoStreamScreen()
                        }
                    }
                }
            }
        }
    }
}
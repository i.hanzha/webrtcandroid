package com.example.webrtcstream

import android.app.Application
import io.getstream.log.android.AndroidStreamLogger

class App: Application() {

    override fun onCreate() {
        super.onCreate()

        AndroidStreamLogger.installOnDebuggableApp(this)
    }
}